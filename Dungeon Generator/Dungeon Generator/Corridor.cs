﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Dungeon_Generator
{
    public class Corridor
    {
        public List<Corridor> Connections { get; set; }
        public Corridor Parrent { get; set; }
        public bool IsConnected { get; set; } //TODO: Maybe only a getter???
        public bool IsDeadEnd { get; set; }
        public Point Possition { get; set; }

        public Corridor(Point _pos)
        {
            Possition = _pos;
        }

        public Corridor(Point _pos, Corridor _corridor = null)
        {
            Possition = _pos;
            Parrent = _corridor;
        }

        public bool HasParrent()
        {
            return Parrent != null;
        }
    }
}
