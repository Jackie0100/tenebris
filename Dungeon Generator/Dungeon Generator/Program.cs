﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Dungeon_Generator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(100, 45);
            Console.SetWindowPosition(0, 0); 
            do
            {
                Console.Clear();
                Dungeon d = new Dungeon(30, 30, 4, 8, 30, 50, RoomLayouts.Dense, CorridorLayouts.Straight);
                d.DrawDungeon();
            }
            while (Console.ReadKey().KeyChar != ' ');
        }
    }
}
