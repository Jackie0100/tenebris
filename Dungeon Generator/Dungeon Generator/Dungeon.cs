﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Dungeon_Generator
{
    public class Dungeon
    {
        int Seed { get; set; }
        public int Rows { get; private set; }
        public int Cols { get; private set; }
        DungeonLayouts DungeonLayout { get; set; }
        int RoomMin { get; set; }
        int RoomMax { get; set; }
        int room_base { get; set; }
        int room_radix { get; set; }
        
        RoomLayouts RoomLayout { get; set; }
        CorridorLayouts CorridorLayout { get; set; }
        int DeadEndRemoval { get; set; }
        int NumbersofStairs { get; set; }
        int NumbersofRooms { get; set; }

        public Room[] Rooms { get; private set; }

        public Tile[,] Tiles { get; private set; }

        public Random Rand { get; private set; }

        public Room this[int x]
        {
            get { return Rooms[x]; }
            set { Rooms[x] = value; }
        }

        public Tile this[int x, int y]
        {
            get { return Tiles[x, y]; }
            set { Tiles[x, y] = value; }
        }

        public Dungeon(int _cols, int _rows, int _minroom, int _maxroom, int _minnumroom, int _maxnumroom, RoomLayouts _roomlayout, CorridorLayouts _corridorlayouts)
        {
            Seed = new Random().Next();
            Rand = new Random(Seed);

            Rows = _rows;
            Cols = _cols;
            RoomMin = _minroom;
            RoomMax = _maxroom;
            RoomLayout = _roomlayout;
            NumbersofRooms = Rand.Next(_minnumroom, _maxnumroom);
            room_base = (RoomMin + 1) / 2;
            room_radix = ((RoomMax - RoomMin) / 2) + 1;
            CorridorLayout = _corridorlayouts;
            Tiles = new Tile[Cols, Rows];
            Rooms = new Room[NumbersofRooms];
            InitTiles();
            PlaceRooms();
            PlaceDoors();
            //PlaceCorridors();
        }

        private void InitTiles()
        {
            for (int r = 0; r < Rows; r++)
            {
                for (int c = 0; c < Cols; c++)
                {
                    Tiles[c, r] = new Tile(CellTypes.NOTHING);
                }
            }
            SquareMask();
        }

        #region Rooms
        private void PlaceRooms()
        {
            PackedRooms();
        }

        private void PackedRooms()
        {
            //for (int i = 0; i < Rows; i++)
            //{
            //    int r = (i * 2) / 2;
            //    for (int j = 0; j < Cols; j++)
            //    {
            //        int c = ((j * 2) / 2);
            //        if (Tiles[r, c].CellType == CellTypes.ROOM)
            //            continue;
            //        if ((i == 0 || j == 0) && Rand.Next(2) != 0)
            //            continue;

            PlaceRoom();
            //    }
            //}
        }

        private void ScatteredRooms()
        {
        }

        bool replace = true;
        const int replacetries = 10;
        private void PlaceRoom()
        {
            for (int i = 0; i < NumbersofRooms; i++)
            {
                Room room = new Room(Cols, Rows, RoomMin, RoomMax, (int)RoomLayout, Rand);

                if (room.X < 1 || room.X > Cols)
                    return;
                if (room.Y < 1 || room.Y > Rows)
                    return;
                int rt = 0;
                do
                {
                    if (room.CheckRoomOverlay(this, true))
                    {
                        Rooms[i] = room;
                        for (int r = room.Y - room.OffSet; r <= room.Y + room.Height + room.OffSet; r++)
                        {
                            if (r < 1 || r > Rows - 2)
                                continue;

                            for (int c = room.X - room.OffSet; c <= room.X + room.Width + room.OffSet; c++)
                            {
                                if (c < 1 || c > Cols - 2)
                                    continue;

                                if ((c < room.X || c > room.X + room.Width) || (r < room.Y || r > room.Y + room.Height))
                                {
                                    this[c, r].CellType = CellTypes.PERIMETER;
                                }
                                else
                                {
                                    this[c, r].CellType = (CellTypes)(CellTypes.ROOM_ID | (CellTypes)i + 1);
                                }
                            }
                        } 
                        replace = false;
                    }
                    else
                    {
                        room = new Room(Cols, Rows, RoomMin, RoomMax, (int)RoomLayout, Rand);
                    }
                    rt++;
                } while (replace && rt < replacetries);
                replace = true;
            }
        }

        public void PlaceDoors()
        {
            foreach (Room r in Rooms)
            {
                if (r == null)
                    continue;
                do
                {
                    int num = Rand.Next(1, 16);

                    if ((num & ((int)Direction.North)) == (int)Direction.North && r.Y > 5)
                    {
                        Door d = new Door (Direction.North, new Point(Rand.Next(r.X + 1, r.X + r.Width - 1), r.Y));
                        r.Doors.Add(d);
                        Tiles[d.Position.X, d.Position.Y] = new Tile(CellTypes.DOOR);
                    }
                    if ((num & ((int)Direction.South)) == (int)Direction.South && r.Y + r.Height < Rows - 5)
                    {
                        Door d = new Door(Direction.South, new Point(Rand.Next(r.X + 1, r.X + r.Width - 1), r.Y + r.Height));
                        r.Doors.Add(d);
                        Tiles[d.Position.X, d.Position.Y] = new Tile(CellTypes.DOOR);
                    }
                    if ((num & ((int)Direction.East)) == (int)Direction.East && r.X + r.Width < Cols - 5)
                    {
                        Door d = new Door(Direction.East, new Point(r.X + r.Width, Rand.Next(r.Y + 1, r.Y + r.Height - 1)));
                        r.Doors.Add(d);
                        Tiles[d.Position.X, d.Position.Y] = new Tile(CellTypes.DOOR);
                    }
                    if ((num & ((int)Direction.West)) == (int)Direction.West && r.X > 5)
                    {
                        Door d = new Door(Direction.West, new Point(r.X, Rand.Next(r.Y + 1, r.Y + r.Height - 1)));
                        r.Doors.Add(d);
                        Tiles[d.Position.X, d.Position.Y] = new Tile(CellTypes.DOOR);
                    }
                } while (r.Doors.Count == 0); //TODO:Optimazed this kind ofbad pratice "Retry" programming!
            }
        }
        #endregion
        #region Doors

        public void OpenRooms()
        {
            foreach (Room room in Rooms)
            {
                OpenRoom(room);
            }
        }

        public void OpenRoom(Room room)
        {

        }

        #endregion

        #region Corridors

        public void PlaceCorridors()
        {
            //for (int i = 0; i < Rooms.Count(); i++)
            //{
            //    if (Rooms[i] != null)
            //        Tunnel(Rooms[i].Doors[0].StartPosition, Rooms[i].Doors[0].Dir, i   + 1);
            //}
            for (int i = 1; i < Rows - 1; i++)
            {
                for (int j = 1; j < Cols - 1; j++)
                {
                    if (this[j, i].CellType == CellTypes.NOTHING || this[j, i].CellType == CellTypes.PERIMETER)
                        Tunnel(new Point(j, i), Direction.None, i + j);
                }
            }
        }

        public void Tunnel(Point p, Direction lastdir, int bit)
        {
            List<Direction> dirs = TunnelDir(p);

            if (this[p.X, p.Y].CellType == CellTypes.PERIMETER || this[p.X, p.Y].CellType == CellTypes.NOTHING)
            {
                this[p.X, p.Y].CellType = CellTypes.CORRIDOR | (CellTypes)bit;
            }

            if (dirs.Contains(Direction.None))
            {
                return;
            }

            Direction dir = lastdir;

            if (dirs.Contains(lastdir))
            {
                if (Rand.Next(0, 100) < (int)CorridorLayout)
                {
                    dir = dirs[Rand.Next(0, dirs.Count)];
                }
            }
            else
            {
                dir = dirs[Rand.Next(0, dirs.Count)];
            }

            if (dir == Direction.North)
            {
                Tunnel(new Point(p.X, p.Y - 1), dir, bit);
            }
            if (dir == Direction.South)
            {
                Tunnel(new Point(p.X, p.Y + 1), dir, bit);
            }
            if (dir == Direction.West)
            {
                Tunnel(new Point(p.X - 1, p.Y), dir, bit);
            }
            if (dir == Direction.East)
            {
                Tunnel(new Point(p.X + 1, p.Y), dir, bit);
            }
        }

        public List<Direction> TunnelDir(Point p)
        {
            List<Direction> d = new List<Direction>();

            if (this[p.X, p.Y - 1].CellType == CellTypes.NOTHING || this[p.X, p.Y - 1].CellType == CellTypes.PERIMETER)
            {
                d.Add(Direction.North);
            }
            //else if (this[p.X, p.Y - 1].CellType == CellTypes.DOOR)
            //{
            //    d.Add(Direction.None);
            //    return d;
            //}
            if (this[p.X, p.Y + 1].CellType == CellTypes.NOTHING || this[p.X, p.Y + 1].CellType == CellTypes.PERIMETER)
            {
                d.Add(Direction.South);
            }
            //else if (this[p.X, p.Y + 1].CellType == CellTypes.DOOR)
            //{
            //    d.Add(Direction.None);
            //    return d;
            //}
            if (this[p.X - 1, p.Y].CellType == CellTypes.NOTHING || this[p.X - 1, p.Y].CellType == CellTypes.PERIMETER)
            {
                d.Add(Direction.West);
            }
            //else if (this[p.X - 1, p.Y].CellType == CellTypes.DOOR)
            //{
            //    d.Add(Direction.None);
            //    return d;
            //}
            if (this[p.X + 1, p.Y].CellType == CellTypes.NOTHING || this[p.X + 1, p.Y].CellType == CellTypes.PERIMETER)
            {
                d.Add(Direction.East);
            }
            //else if (this[p.X + 1, p.Y].CellType == CellTypes.DOOR)
            //{
            //    d.Add(Direction.None);
            //    return d;
            //}
            if (d.Count == 0)
            {
                d.Add(Direction.None);
            }

            return d;
        }

        #endregion

        //private void CheckRoomOverlay(Room room)
        //{
        //    for (int r = room.Y; r <= room.Y + room.Height; r++)
        //    {
        //        for (int c = room.X; c <= room.X + room.Width; c++)
        //        {
        //            //if (Tiles[r,c].CellType == CellTypes.NOTHING)
        //            //{
        //            //    Tiles[r, c].CellType = CellTypes.BLOCKED;
        //            //}
        //            if (Tiles[r,c].CellType == CellTypes.NOTHING)
        //            {
        //                Tiles[r, c].CellType = CellTypes.ROOM_ID;
        //            }
        //        }
        //    }
        //}
        

        #region Masks
        private void SquareMask()
        {
            float y = 3f / Cols;
            float x = 3f / Rows;
            for (int c = 0; c < Cols; c++)
            {
                for (int r = 0; r < Rows; r++)
                {
                    if (r == 0 || r == Tiles.GetLength(1) - 1 || c == 0 || c == Tiles.GetLength(0) - 1)
                    {
                        Tiles[c, r] = new Tile(CellTypes.BLOCKED);
                    }
                }
            }
        }

        public void RoundMask()
        {
            int center_r = (int)(Rows / 2);
            int center_c = (int)(Cols / 2);

            if (Rows < Cols)
            {
                for (int r = 0; r < Rows; r++)
                {
                    for (int c = 0; c < Cols; c++)
                    {
                        int d = (int)Math.Sqrt(Math.Pow((r - center_r), 2) + (Math.Pow((c - center_c) * (double)(Rows - 1) / (double)(Cols - 1), 2)));
                        if (d + 1 > center_r || d + 1 > center_c)
                            Tiles[c, r] = new Tile(CellTypes.BLOCKED);
                    }
                }
            }
            else if (Rows > Cols)
            {
                for (int r = 0; r < Rows; r++)
                {
                    for (int c = 0; c < Cols; c++)
                    {
                        int d = (int)Math.Sqrt(Math.Pow((r - center_r) * (double)(Cols - 1) / (double)(Rows - 1), 2) + (Math.Pow((c - center_c), 2)));
                        if (d + 1 > center_r || d + 1 > center_c)
                            Tiles[r, c] = new Tile(CellTypes.BLOCKED);
                    }
                }
            }
            else
            {
                for (int r = 0; r < Rows; r++)
                {
                    for (int c = 0; c < Cols; c++)
                    {
                        int d = (int)Math.Sqrt(Math.Pow((r - center_r), 2) + (Math.Pow((c - center_c), 2)));
                        if (d + 1 > center_r || d + 1 > center_c)
                            Tiles[r, c] = new Tile(CellTypes.BLOCKED);
                    }
                }
            }
        }
        #endregion

        //private class Rectangle
        //{
        //    public int X { get; set; }
        //    public int Y { get; set; }
        //    public int Width { get; set; }
        //    public int Height { get; set; }

        //    public Rectangle(int col, int row, int min, int max, Random rand)
        //    {
        //        Height = rand.Next(min + 1, max + 1);
        //        Width = rand.Next(min + 1, max + 1);

        //        Y = rand.Next(1, row - Height);
        //        X = rand.Next(1, col - Width);
        //    }

        //    public Rectangle(int x, int y, int w, int h)
        //    {
        //        X = x;
        //        Y = y;
        //        Width = w;
        //        Height = h;
        //    }
        //}
    }
        
    public enum DungeonLayouts
    {
        Square,
        Rectangle,
        Round,
    }

    public enum RoomLayouts
    {
        Sparse = 3,
        Dense = 1,
        Complex,
    }

    public enum CorridorLayouts
    {
        Labyrinth = 100,
        Bent = 50,
        Straight = 5,
    }

    public enum Direction
    {
        None = 0, North = 1, South = 2, East = 4, West = 8,
    }
}
