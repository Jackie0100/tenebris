﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Dungeon_Generator
{
    public static class ExtentionMethods
    {
        public static double Distance(this System.Drawing.Point point1, System.Drawing.Point point2)
        {
            return (Math.Sqrt(Math.Pow(point2.X - point1.X, 2) + Math.Pow(point2.Y - point1.Y, 2)));
        }

        public static void DrawDungeon(this Dungeon d)
        {
            Console.Clear();
            for (int y = 0; y < d.Rows; y++)
            {
                for (int x = 0; x < d.Cols; x++)
                {
                    if (d[x, y].CellType == CellTypes.BLOCKED)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write("#");
                    }
                    else if (d[x, y].CellType.HasFlag(CellTypes.ROOM_ID))
                    {
                        Console.ForegroundColor = (ConsoleColor)(((int)(d[x, y].CellType ^ CellTypes.ROOM_ID) % 15) + 1);
                        Console.Write("*");
                    }
                    else if (d[x, y].CellType == CellTypes.DOOR)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write("+");
                    }
                    //else if (d[x, y].CellType == CellTypes.ROOMOFFSET)
                    //{
                    //    s += "o"; //TODO: DEBUG Values!!!
                    //}
                    else if (d[x, y].CellType.HasFlag(CellTypes.CORRIDOR))
                    {
                        Console.ForegroundColor = (ConsoleColor)(((int)(d[x, y].CellType ^ CellTypes.CORRIDOR) % 15) + 1);
                        Console.Write("C");
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write(" ");
                    }
                }
                Console.Write("\n");
            }
        }
    }
}
