﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Dungeon_Generator
{
    public class Room
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int OffSet { get; set; } //used for packed/nonpacked rooms to spread them slightly from eachother

        public int Size { get { return Width * Height; } }

        public List<Door> Doors { get; set; } //TODO: Change to vector2 in unity3D

        public Room(int col, int row, int min, int max, int offset, Random rand)
        {
            Doors = new List<Door>();
            Height = rand.Next(min + 1, max + 1);
            Width = rand.Next(min + 1, max + 1);

            Y = rand.Next(1, row - Height - 1);
            X = rand.Next(1, col - Width - 1);

            OffSet = offset;
        }

        public Room(int x, int y, int w, int h)
        {
            X = x;
            Y = y;
            Width = w;
            Height = h;
        }

        public bool CheckRoomOverlay(Dungeon dun, bool replace = true)
        {
            //if ((X < 0 || Y < 0) && (X + Width > dun.Rows - 1 || Y + Height > dun.Cols - 1))
            //    return false;

            for (int i = 0; i < Width + OffSet; i++)
            {
                for (int j = 0; j < Height + OffSet ; j++)
                {
                    //if (dun[X + i, Y + j].CellType != CellTypes.NOTHING && !replace)
                    //{
                    //    return false;
                    //}
                    if (dun[X + i, Y + j].CellType != CellTypes.NOTHING)
                    {
                        if (replace)
                        {
                            return MoveRoom(dun);
                        }
                        else
                        {
                            return false;
                        }
                        //return false;
                        //if (i <= dun.Cols / 2 && (X + Width - 1 < dun.Cols && X >= 0))
                        //{
                        //    if (MoveRoom(dun, Direction.East, i, j))
                        //    {
                        //        return true;
                        //    }
                        //}
                        //if (i > dun.Cols / 2 && (X + Width < dun.Cols && X - 1 >= 0))
                        //{
                        //    if (MoveRoom(dun, Direction.West, i, j))
                        //    {
                        //        return true;
                        //    }
                        //}
                        //if (j <= dun.Rows / 2 && (Y + Height < dun.Rows - 1 && Y >= 0))
                        //{
                        //    if (MoveRoom(dun, Direction.South, i, j))
                        //    {
                        //        return true;
                        //    }
                        //}
                        //if (j > dun.Rows / 2 && (Y + Height < dun.Rows && Y - 1 >= 0))
                        //{
                        //    if (MoveRoom(dun, Direction.North, i, j))
                        //    {
                        //        return true;
                        //    }
                        //}
                    }
                }
            }
            return true;
        }

        public bool MoveRoom(Dungeon dun)
        {
            List<int> randcol = new List<int>();
            for (int c = 0; c < dun.Cols - Width; c++)
            {
                randcol.Add(c);
            }
            randcol = randcol.OrderBy(s => dun.Rand.Next()).ToList<int>();
            
            for (int i = 0; i < dun.Cols - Width; i++)
            {
                List<int> randrow = new List<int>();
                for (int r = 0; r < dun.Rows - Height; r++)
                {
                    randrow.Add(r);
                }
                randrow = randrow.OrderBy(s => dun.Rand.Next()).ToList<int>();

                for (int j = 0; j < dun.Rows - Height; j++)
                {
                    bool replaced = true;
                    if (dun[randcol[i], randrow[j]].CellType == CellTypes.NOTHING)
                    {
                        bool outerbreak = false;
                        
                        for (int k = 0; k < Width + OffSet; k++)
                        {
                            for (int l = 0; l < Height + OffSet; l++)
                            {
                                if (dun[randcol[i] + k, randrow[j] + l].CellType == CellTypes.NOTHING)
                                {
                                    replaced = true;
                                }
                                else
                                {
                                    replaced = false;
                                    outerbreak = true;
                                    break;
                                }
                            }
                            if (outerbreak)
                            {
                                break;
                            }
                        }
                        if (replaced)
                        {
                            X = randcol[i];
                            Y = randrow[j];
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        //public bool MoveRoom(Dungeon dun, Direction dir, int x, int y)
        //{
        //    Console.WriteLine("Room Moved from " + X + ";" + Y);
        //    while (dun[X + x, Y + y].CellType != CellTypes.NOTHING)
        //    {
        //        if ((X < 0 || Y < 0 || X + Width > dun.Rows - 1 || Y + Height > dun.Cols - 1))
        //            return false;

        //        if (dir == Direction.North)
        //        {
        //            Y--;
        //        }
        //        if (dir == Direction.South)
        //        {
        //            Y++;
        //        }
        //        if (dir == Direction.West)
        //        {
        //            X--;
        //        }
        //        if (dir == Direction.East)
        //        {
        //            X++;
        //        }

        //        if ((X < 0 || Y < 0 || X + Width > dun.Rows - 1 || Y + Height > dun.Cols - 1))
        //            return false;
        //    }
        //    Console.WriteLine("Room Moved to " + X + ";" + Y);
        //    //Console.ReadKey();
        //    return true;
        //}
    }
}
