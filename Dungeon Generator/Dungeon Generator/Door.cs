﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Dungeon_Generator
{
    public class Door
    {
        public Direction Dir { get; set; }
        public Point Position { get; set; }
        public Point StartPosition
        {
            get
            {
                if (Dir == Direction.North)
                    return new Point(Position.X, Position.Y - 1);
                else if (Dir == Direction.South)
                    return new Point(Position.X, Position.Y + 1);
                else if (Dir == Direction.West)
                    return new Point(Position.X - 1, Position.Y);
                else if (Dir == Direction.East)
                    return new Point(Position.X + 1, Position.Y);
                else
                    return Position;
            }
        }

        public Door(Direction _dir, Point _pos)
        {
            Dir = _dir;
            Position = _pos;
        }
    }
}
