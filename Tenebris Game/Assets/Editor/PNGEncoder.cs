﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
//using System;

public class PNGEncoder : MonoBehaviour
{
    [MenuItem("Assets/Encode to PNG")]
    static void Apply ()
    {
        Object[] objects  = Selection.GetFiltered(typeof(Texture2D),  SelectionMode.Editable | SelectionMode.TopLevel);
        if (objects == null)
        {
            EditorUtility.DisplayDialog("Select Texture", "You Must Select a Texture first!", "Ok");
            return;
        }
        foreach (Object o in objects)
        {
            try
            {

                Texture2D texture = (Texture2D)(o as Texture2D);
                Texture2D tex = new Texture2D(texture.width, texture.height, TextureFormat.ARGB32, false);
                tex.SetPixels32(texture.GetPixels32());
                var bytes = tex.EncodeToPNG();
                File.WriteAllBytes(Application.dataPath + "/" + texture.name + ".png", bytes);
            }
            catch (System.Exception e)
            {
                Debug.Log(e.ToString());
            }
        }
    }
}
