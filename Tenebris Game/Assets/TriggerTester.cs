using UnityEngine;
using System.Collections;

public class TriggerTester : MonoBehaviour
{
    void Update()
    {
        this.transform.Rotate((Vector3.up * 100) * Time.deltaTime);
    }

    void OnCollisionEnter(Collision hit)
    {
        //Debug.Log("ENTER: " + hit.transform.name);
    }

    void OnTriggerEnter(Collider hit)
    {
        Debug.Log("ENTER: " + hit.name);
    }

    void OnTriggerStay(Collider hit)
    {
        //Debug.Log("STAY: " + hit.name);
    }

    void OnTriggerExit(Collider hit)
    {
        Debug.Log("EXIT: " + hit.name);
    }
}
