﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Stats
{
    public int stamina;
    public int intellect;
    public int spirit;
    public int agility;
    public int strength;
    public int armor;
    public int resistance;
    public int parry;
    public int dodge;
    public int defence;
    public int block;
    public int attackPower;
    public int armorPenetration;
    public int meleeHit;
    public int meleeCritical;
    public int rangedPower;
    public int rangedHit;
    public int rangedCritical;
    public int spellPower;
    public int spellCritical;
    public int spellHit;
    public int spellPenetration;
    public int healthRegain;
    public int manaRegain;
}
