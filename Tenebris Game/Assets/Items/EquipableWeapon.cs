﻿using System;
using UnityEngine;

public class Weapon : EquipableItem
{
    public WeaponType weaponType { get; set; }
    public int minDamage { get; set; }
    public int maxDamage { get; set; }
    public float speed { get; set; }
    public float DPS { get; set; }
    public int equipEffectID { get; set; }
    public int chanceonHitID { get; set; }

    public Weapon(string _name, string _itemText, RarityType _rareType, Texture2D _icon, int _value, int _itemLevel, int _usableLevel, int _stackSize,
        int _useEffect, int _charges, bool _destroyuponlastuse, EquipmentSlot _equipmentSlot, int _durability, int _maxDurability, Stats _stats, 
        WeaponType _weaponType, int _minDamage, int _maxDamage, float _speed, int _equipEffectID, int _chanceonHitID)
        : base(_name, _itemText, _rareType, _icon, _value, _itemLevel, _usableLevel, _stackSize, _useEffect, _charges, 
        _destroyuponlastuse, _equipmentSlot, _durability, _maxDurability, _stats)
    {
        weaponType = _weaponType;
        minDamage = _minDamage;
        maxDamage = _maxDamage;
        speed = _speed;
        equipEffectID = _equipEffectID;
        chanceonHitID = _chanceonHitID;
        DPS = (float)((minDamage + maxDamage) / 2) / speed;
    }
}
