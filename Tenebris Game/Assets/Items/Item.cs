﻿using System;
using UnityEngine;

public class Items
{
    public string Name { get; set; }
    public string ItemText { get; set; }
    public RarityType RareType { get; set; }
    public Texture2D Icon { get; set; }
    public int Value { get; set; }
    public int ItemLevel { get; set; }
    public int UsableLevel { get; set; }
    public int StackSize { get; set; }

    public Items(string _name, string _itemText, RarityType _rareType, Texture2D _icon, int _value, int _itemLevel, int _usableLevel, int _stackSize)
    {
        Name = _name;
        ItemText = _itemText;
        RareType = _rareType;
        Icon = _icon;
        Value = _value;
        ItemLevel = _itemLevel;
        UsableLevel = _usableLevel;
        StackSize = _stackSize;
    }

    public virtual void Use()
    {
    }
}
