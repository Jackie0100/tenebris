﻿using System;

public enum RarityType
{
    Poor, Common, Uncommon, Rare, Epic, Legendary, Artifact,
}
