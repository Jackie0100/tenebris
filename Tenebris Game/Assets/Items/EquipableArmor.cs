﻿using System;
using UnityEngine;

public class Armor : EquipableItem
{
    public ArmorType armorType { get; set; }
    public int equipEffectID { get; set; }
    public int chanceWhenHittedEffectID { get; set; }

    public Armor(string _name, string _itemText, RarityType _rareType, Texture2D _icon, int _value, int _itemLevel, int _usableLevel, int _stackSize,
        int _useEffect, int _charges, bool _destroyuponlastuse, EquipmentSlot _equipmentSlot, int _durability, int _maxDurability, Stats _stats, 
        ArmorType _armorType, int _equipEffectID, int _chanceWhenHittedEffectID)
        : base(_name, _itemText, _rareType, _icon, _value, _itemLevel, _usableLevel, _stackSize, _useEffect, _charges, _destroyuponlastuse,
        _equipmentSlot, _durability, _maxDurability, _stats)
    {
        armorType = (ArmorType)_armorType;
        equipEffectID = _equipEffectID;
        chanceWhenHittedEffectID = _chanceWhenHittedEffectID;
    }
}
