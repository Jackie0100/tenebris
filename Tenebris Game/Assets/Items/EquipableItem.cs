﻿using System;
using UnityEngine;

public class EquipableItem : Usable
{
    public EquipmentSlot equipmentSlot { get; set; }
    public int durability { get; set; }
    public int maxDurability { get; set; }
    public Stats stats { get; set; }

    public EquipableItem(string _name, string _itemText, RarityType _rareType, Texture2D _icon, int _value, int _itemLevel, int _usableLevel, int _stackSize,
        int _useEffect, int _charges, bool _destroyuponlastuse, EquipmentSlot _equipmentSlot, int _durability, int _maxDurability, Stats _stats)
        : base(_name, _itemText, _rareType, _icon, _value, _itemLevel, _usableLevel, _stackSize, _useEffect, _charges, _destroyuponlastuse)
    {
        equipmentSlot = _equipmentSlot;
        durability = _durability;
        maxDurability = _maxDurability;
        stats = _stats;
    }
}
