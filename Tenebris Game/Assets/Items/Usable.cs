﻿using System;
using UnityEngine;

public class Usable : Items
{
    public int UseEffect { get; set; }
    public int Charges { get; set; }
    public bool DestroyUponLastUse { get; set; }

    public Usable(string _name, string _itemText, RarityType _rareType, Texture2D _icon, int _value, int _itemLevel, int _usableLevel, int _stackSize,
        int _useEffect, int _charges, bool _destroyuponlastuse)
        : base (_name, _itemText, _rareType, _icon, _value, _itemLevel, _usableLevel, _stackSize)
    {
        UseEffect = _useEffect;
        Charges = _charges;
        DestroyUponLastUse = _destroyuponlastuse;
    }
}
