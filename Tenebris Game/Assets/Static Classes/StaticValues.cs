using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Static values the have no real place to be.
/// </summary>
public static class StaticValues
{
    public static string MouseAxisX = "None";
    public static string MouseAxisY = "None";
    public static int invertX = 1;



    public static int invertY = 1;
    public static float deadZone = 0.25f;
    public static float AxisxSpeed = 100;
    public static float AxisySpeed = 100;

    /// <summary>
    /// makes a list into a single string which is used for gui e.g.
    /// </summary>
    /// <param name="l">A list</param>
    /// <returns>Returns a string with everything from the list</returns>
    public static string GetListContentAsString(this List<KeyCode> l)
    {
        string str = "";
        for (int i = 0; i < l.Count; i++)
        {
            str += l[i].ToString();
            if (i + 1 != l.Count)
            {
                str += " + ";
            }
        }
        return str;
    }

    public static bool HasParent(this Transform t)
    {
        return t.parent != null;
    }

    public static Transform GetSuperParent(this Transform t)
    {
        if (t.HasParent())
        {
            return t.parent.GetSuperParent();
        }
        else
        {
            return t;
        }
    }

    public static List<T> GetAllChildsWithComponent<T>(this Transform t)
    {
        List<T> ret = new List<T>();
        if (t.HasParent())
        {
            ret.AddRange(t.parent.GetAllChildsWithComponent<T>());
        }
        else
        {
            ret.AddRange(t.GetComponentsInChildren(typeof(T)).OfType<T>().ToList());
        }
        return ret;
    }

    public static void SetAveragePosition(this Vector3 vec, Transform[] trans)
    {
        Vector3 pos = new Vector3();
        int count = 0;
        foreach (Transform t in trans)
        {
            pos += t.position;
            count++;
        }
        pos /= count;
        vec = pos;
    }

    public static void SetAveragePosition(this Vector3 vec, GameObject[] gos)
    {
        Vector3 pos = new Vector3();
        int count = 0;
        foreach (GameObject g in gos)
        {
            pos += g.transform.position;
            count++;
        }
        pos /= count;
        vec = pos;
    }
}