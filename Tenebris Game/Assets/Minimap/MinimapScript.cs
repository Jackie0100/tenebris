using UnityEngine;
using System.Collections.Generic;

public class MinimapScript : MonoBehaviour
{
    public Transform target;
    public bool rotateWithTarget = true;

	void Update ()
    {
        if (target)
        {
            Vector3 newPos = target.position;
            newPos.y += 300;
            transform.position = newPos;
            if (rotateWithTarget)
            {
                transform.eulerAngles = new Vector3(90, 0, 0);
            }
            else
            {
                transform.eulerAngles = new Vector3(90, 0, target.eulerAngles.y * -1);
            }
        }
	}
}
