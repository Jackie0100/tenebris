using UnityEngine;
using System;
using System.Collections;
using TerrainGenerator;

public class SimpleTerrainGenerator : MonoBehaviour
{
    public GameObject[] go;
    public int[] percentage;
    public int seed;
    public HeightMap HeightMap;
    public BiomeMap BiomeMap;
    public Terrain ThisTerrain;
    public GameObject tester;
    public Terrain TerTop;
    public Terrain TerLeft;
    public Terrain TerRight;
    public Terrain TerBottom;

    // Use this for initialization
    void Start ()
    {
        PrepareTerrain();

        //TODO: Build this up clean and nice :D n^2 function
        //float[,,] splat = new float[1024,1024,2];
        //for (int x = 0; x < 1024; x++)
        //    for (int y = 0; y < 1024; y++)
        //    {
        //        splat[x, y, 0] = 0.5f;
        //        splat[x, y, 1] = 0.5f;
        //    }

        //ThisTerrain.terrainData.SetAlphamaps(0, 0, splat);
    }

    public void PrepareTerrain()
    {
        ThisTerrain = GetComponent<Terrain>();
        //BiomeMap = new BiomeMap(ThisTerrain.terrainData.alphamapHeight);
        //BiomeMap.SetNoise(2);
        //BiomeMap.Smoothen();
        //BiomeMap.Normalize();
        //ThisTerrain.terrainData.SetAlphamaps(0, 0, TerrainTools.TerrainTexture(ThisTerrain.terrainData.alphamapHeight, ThisTerrain.terrainData.alphamapHeight, 4, BiomeMap));
        
        HeightMap = new HeightMap(ThisTerrain.terrainData.heightmapHeight);
        HeightMap.SetNoise(0.25f);
        HeightMap.Perturb(32, 32);
        HeightMap.Erode(8);
        for (int i = 0; i < 8; i++)
            HeightMap.Smoothen();
        HeightMap.Normalize();
        new WaitForEndOfFrame();
        ThisTerrain.terrainData.SetHeights(0, 0, HeightMap.Heights);
        ThisTerrain.SetNeighbors(TerLeft, TerTop, TerRight, TerBottom);
        if (TerLeft != null)
        {
            StartCoroutine(TerrainTools.StitchTerrains(ThisTerrain, TerLeft, 128, 0, 20));
        }
        if (TerTop != null)
        {
            StartCoroutine(TerrainTools.StitchTerrains(ThisTerrain, TerTop, 128, 0, 20));
        }
        if (TerRight != null)
        {
            StartCoroutine(TerrainTools.StitchTerrains(ThisTerrain, TerRight, 128, 0, 20));
        }
        if (TerBottom != null)
        {
            StartCoroutine(TerrainTools.StitchTerrains(ThisTerrain, TerBottom, 128, 0, 20));
        }
        //GetComponent<GraphUpdateScene>().Apply();
        ////new AstarPath().
        //GameObject.Find("A*").GetComponent<AstarPath>().Scan();
        //Debug.Log(GameObject.Find("A*").GetComponent<AstarPath>().graphTypes[0].ToString());
    }
}
