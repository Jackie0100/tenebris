using UnityEngine;
using System.Collections;

public interface IInteract
{
    void Interact(GameObject go);
}
