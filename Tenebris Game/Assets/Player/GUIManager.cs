using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tenebris.Player.Hero;

public class GUIManager : MonoBehaviour
{
	bool changekey = false;
	string tochangecommand;
	KeyStoreType tochangekst;
	InputManager ih;
	private int a = 0;
	public GUISkin skin;

	void Start()
	{
		ih = this.GetComponent<MMOController>().inputManager;
	}

	void OnGUI()
	{
		GUI.Window(0, new Rect(Screen.width / 2 - 210, Screen.height / 16, 440, 250), DoInputWindow, "Inputs");
		if (changekey)
		{
			changekey = !ih.ChangeCurrentKeyInput(tochangecommand, tochangekst);
		}
	}

	void DoInputWindow(int id)
	{
		GUI.Label(new Rect(110, 20, 100, 25), "Primary Input");
		GUI.Label(new Rect(220, 20, 100, 25), "Secondary Input");
		GUI.Label(new Rect(330, 20, 100, 25), "Joystick Input");
		int i = 0;
		int j = 0;
		string s = "";
		for (i = 0; i < ih.Commands.Count; i++)
		{
			if (s != ih.InputKeys[i].TagCategory)
			{
				s = ih.InputKeys[i].TagCategory;
				GUI.Label(new Rect(5, 20 + (25 * (i + 1)) + (25 * j), 100, 25), ih.InputKeys[i].TagCategory);
				j++;
			}
			GUI.Label(new Rect(5, 20 + (25 * (i + 1)) + (25 * j), 100, 25), ih.InputKeys[i].Command);
			if (GUI.Button(new Rect(110, 20 + (25 * (i + 1)) + (25 * j), 100, 25), ih.InputKeys[i].Key1.GetListContentAsString()))
			{
				tochangecommand = ih.InputKeys[i].Command;
				tochangekst = KeyStoreType.Key1;
				changekey = true;
			}
			if (GUI.Button(new Rect(220, 20 + (25 * (i + 1)) + (25 * j), 100, 25), ih.InputKeys[i].Key2.GetListContentAsString()))
			{
				tochangecommand = ih.InputKeys[i].Command;
				tochangekst = KeyStoreType.Key2;
				changekey = true;
			}
			if (GUI.Button(new Rect(330, 20 + (25 * (i + 1)) + (25 * j), 100, 25), ih.InputKeys[i].Joystick))
			{
				tochangecommand = ih.InputKeys[i].Command;
				tochangekst = KeyStoreType.Joystick;
				changekey = true;
			}
		}
		GUI.Label(new Rect(5, 20 + (25 * (i + 1)) + (25 * j), 100, 25), "Mouse Axis");
		a = Mathf.RoundToInt(GUI.HorizontalSlider(new Rect(110, 20 + (25 * (i + 1)) + (25 * j) + 5, 320, 25), a, 0, 4));
		StaticValues.MouseAxisX = "Axis" + a + "X";
		StaticValues.MouseAxisY = "Axis" + a + "Y";
	}
}
