﻿using UnityEngine;
using System.Collections;

namespace Tenebris
{
    namespace DungeonEditor
    {
        public class DungeonEditor : MonoBehaviour
        {
            public Texture2D SelectionHighLight = null;
            public static Rect SelectionArea = new Rect(0, 0, 0, 0);
            private Vector3 StartPoint = -Vector3.one;

            // Update is called once per frame
            void Update()
            {
                CheckDungeon();
            }

            private void CheckDungeon()
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    StartPoint = Input.mousePosition;
                }
                else if (Input.GetKeyUp(KeyCode.Mouse0))
                {
                    StartPoint = -Vector3.one;
                }

                if (Input.GetKey(KeyCode.Mouse0))
                {
                    SelectionArea = new Rect(StartPoint.x, Screen.height - StartPoint.y, Input.mousePosition.x - StartPoint.x,
                        (Screen.height - Input.mousePosition.y) - (Screen.height - StartPoint.y));

                    if (SelectionArea.width < 0)
                    {
                        SelectionArea.x += SelectionArea.width;
                        SelectionArea.width = -SelectionArea.width;
                    }
                    if (SelectionArea.height < 0)
                    {
                        SelectionArea.y += SelectionArea.height;
                        SelectionArea.height = -SelectionArea.height;
                    }
                }
            }

            void OnGUI()
            {
                if (StartPoint != -Vector3.one)
                {
                    GUI.color = new Color(1, 1, 1, 0.5f);
                    GUI.DrawTexture(SelectionArea, SelectionHighLight);
                }
            }
        }
    }
}