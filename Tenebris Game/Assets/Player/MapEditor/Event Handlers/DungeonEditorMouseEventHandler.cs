﻿using UnityEngine;
using System.Collections;

namespace Tenebris
{
    namespace DungeonEditor
    {
        public class DungeonEditorMouseEventHandler : MonoBehaviour
        {
            private Color MainColor { get; set; }
            private Color MainSpecColor { get; set; }

            private Color SelectedColor = Color.green;
            private Color PreSelectedColor = Color.yellow;
            private Color CorrectColor = Color.green;
            private Color InvalidColor = Color.red;

            private bool selected = false;
            private bool preselected = false;
            private Grid Grid { get; set; }
            private bool AmIBeingDraged { get; set; }

            //public bool MouseOver
            //{
            //    get
            //    {
            //        return mouseover;
            //    }
            //    set
            //    {
            //        mouseover = value;
            //        if (value && !selected)
            //            GetAllObjects(PreSelectedColor, PreSelectedColor);
            //        else if (!selected)
            //            GetAllObjects(MainColor, MainSpecColor);
            //    }
            //}

            public bool Selected
            {
                get
                {
                    return selected;
                }
                set
                {
                    if (value)
                    {
                        GetAllObjects(SelectedColor, SelectedColor, true, false);
                        selected = value;
                        if (!Grid.SelectedGo.Contains(transform.GetSuperParent().gameObject))
                            Grid.SelectedGo.Add(transform.GetSuperParent().gameObject);
                    }
                    else
                    {
                        selected = value;
                        GetAllObjects(MainColor, MainSpecColor, false, false);
                    }
                }
            }

            public bool PreSelected
            {
                get
                {
                    return preselected;
                }
                set
                {
                    if (value)
                        GetAllObjects(PreSelectedColor, PreSelectedColor, false, true);
                    else
                        GetAllObjects(MainColor, MainSpecColor, false, false);
                    preselected = value;
                }
            }

            void Awake()
            {
                Grid = GameObject.Find("Grid").GetComponent<Grid>();
            }

            void Start()
            {
                //foreach (Transform t in GetComponentsInChildren<Transform>())
                //{
                //    if (t.name == this.name || t.GetComponents<DungeonEditorMouseEventHandler>() == null)
                //        continue;
                //    t.gameObject.AddComponent<DungeonEditorMouseEventHandler>();
                //}
                if (this.renderer != null)
                {
                    //Grid.IsDraging = false;
                    MainColor = renderer.material.color;
                    MainSpecColor = renderer.material.GetColor("_SpecColor");
                }
            }
            //TODO: Optimaze the collision as mesh collider kill performance (Furture Work)
            void Update()
            {
                if (!Grid.IsDraging)
                {
                    AmIBeingDraged = false;
                }
                else if (Grid.IsDraging && !AmIBeingDraged)
                {
                    return;
                }
                if (Grid.IsDraging && AmIBeingDraged)
                {
                    if (Selected)
                    {
                        Bounds b = new Bounds();
                        foreach (Collider c in transform.GetSuperParent().GetComponentsInChildren<Collider>())
                        {
                            b.Encapsulate(c.bounds);
                        }
                        Grid.MoveObject(Grid.SelectedGo, b.size);
                        if (Grid.IsOverLapping)
                        {
                            foreach (GameObject go in Grid.SelectedGo)
                            {
                                foreach (DungeonEditorMouseEventHandler r in this.transform.GetAllChildsWithComponent<DungeonEditorMouseEventHandler>())
                                {
                                    r.SetOverlayColor(InvalidColor, InvalidColor);
                                }
                            }
                        }
                        else
                        {
                            foreach (GameObject go in Grid.SelectedGo)
                            {
                                foreach (DungeonEditorMouseEventHandler r in this.transform.GetAllChildsWithComponent<DungeonEditorMouseEventHandler>())
                                {
                                    r.SetOverlayColor(SelectedColor, SelectedColor);
                                }
                            }
                        }
                    }
                    return;
                }
                if (renderer != null && (renderer.isVisible && Input.GetKey(KeyCode.Mouse0)) && Vector3.Distance(Grid.StartPoint, Input.mousePosition) > 0.1f)
                {
                    Vector3 campos = Camera.main.WorldToScreenPoint(transform.position);
                    campos.y = Screen.height - campos.y;
                    PreSelected = Grid.SelectionArea.Contains(campos);
                }
                else if (renderer != null && (renderer.isVisible && Input.GetKeyUp(KeyCode.Mouse0)) && Vector3.Distance(Grid.StartPoint, Input.mousePosition) > 0.1f)
                {
                    preselected = false;
                    Vector3 campos = Camera.main.WorldToScreenPoint(transform.position);
                    campos.y = Screen.height - campos.y;
                    if (Input.GetKey(KeyCode.LeftControl))
                    {
                        if (Grid.SelectionArea.Contains(campos))
                        {
                            Selected = true;
                        }
                    }
                    else
                    {
                        Selected = Grid.SelectionArea.Contains(campos);
                    }
                }
            }

            //public void OnMouseDown()
            //{
            //}
            ////TODO: better event handler
            //public void OnMouseUp()
            //{
            //    Debug.Log("TESTER");
            //    if (Input.GetKey(KeyCode.LeftControl))
            //    {
            //        if (!Grid.SelectedGo.Contains(this.gameObject))
            //        {
            //            Selected = true;
            //        }
            //    }
            //    else
            //    {
            //        Grid.SelectedGo.Clear();
            //        Selected = true;
            //    }
            //}

            void OnMouseOver()
            {
                if (Grid.IsDraging)
                {
                    return;
                }
                if (renderer != null && renderer.isVisible && Input.GetKeyUp(KeyCode.Mouse0) && Input.GetKey(KeyCode.LeftControl) && Vector3.Distance(Grid.StartPoint, Input.mousePosition) < 0.1f)
                {
                    if (!Grid.SelectedGo.Contains(this.gameObject))// && preselected)
                    {
                        Selected = true;
                    }
                }
                else if (renderer != null && renderer.isVisible && Input.GetKeyUp(KeyCode.Mouse0) && Vector3.Distance(Grid.StartPoint, Input.mousePosition) < 0.1f)
                {
                    foreach (GameObject g in Grid.SelectedGo)
                    {
                        foreach (DungeonEditorMouseEventHandler d in g.transform.GetAllChildsWithComponent<DungeonEditorMouseEventHandler>())
                        {
                            d.Selected = false;
                        }
                    }
                    if (!Grid.SelectedGo.Contains(this.gameObject))// && preselected)
                    {
                        Grid.SelectedGo.Clear();
                        Selected = true;
                    }
                }
            }

            void OnMouseEnter()
            {
                PreSelected = true;
            }

            void OnMouseExit()
            {
                PreSelected = false;
            }

            void OnMouseDrag()
            {
                if (Selected)
                {
                    AmIBeingDraged = true;
                    Grid.IsDraging = true;
                }
            }

            void GetAllObjects(Color color, Color speccolor, bool _selected, bool _preselected)
            {
                if (Selected)
                    return;

                foreach (DungeonEditorMouseEventHandler r in this.transform.GetAllChildsWithComponent<DungeonEditorMouseEventHandler>())
                {
                    r.selected = _selected;
                    r.preselected = _preselected;
                    r.SetOverlayColor(color, speccolor);
                }
            }

            void SetOverlayColor(Color color, Color speccolor)
            {
                if (renderer != null)
                {
                    renderer.material.color = color;
                    renderer.material.SetColor("_SpecColor", speccolor);
                }
            }
        }
    }
}
