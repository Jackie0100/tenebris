using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Tenebris
{
    namespace Player
    {
        namespace Hero
        {
            [RequireComponent(typeof(CharacterController))]
            [RequireComponent(typeof(CharacterMotor))]

            public class MMOController : MonoBehaviour
            {
                public InputManager inputManager;
                private GameObject character;
                private CharacterMotor motor;
                private MMOCameraController mmocamera;

                private float currentMoveSpeedMultiplier;

                public float runspeedmultiplier = 3;
                public float walkspeedmultiplier = 1;

                public void Awake()
                {
                    currentMoveSpeedMultiplier = runspeedmultiplier;
                    motor = this.GetComponent<CharacterMotor>();
                    inputManager = new InputManager();
                    mmocamera = GameObject.Find("Main Camera").GetComponent<MMOCameraController>();
                    character = this.gameObject;
                    inputManager.UseJoystick = true;
                }

                public void Update()
                {
                    inputManager.CheckControleKeys();
                    Vector3 dir = new Vector3();

                    //Movement statements
                    if (inputManager.CheckInput("Move Forward", KeyStages.Press))
                    {
                        dir.z += 1;
                    }
                    if (inputManager.CheckInput("Move Backward", KeyStages.Press))
                    {
                        dir.z -= 1;
                    }
                    if (inputManager.CheckInput("Strafe Left", KeyStages.Press))
                    {
                        dir.x -= 1;
                    }
                    if (inputManager.CheckInput("Strafe Right", KeyStages.Press))
                    {
                        dir.x += 1;
                    }

                    if (inputManager.CheckInput("Toggle Run/Walk", KeyStages.Down))
                    {
                        if (currentMoveSpeedMultiplier == runspeedmultiplier)
                        {
                            currentMoveSpeedMultiplier = walkspeedmultiplier;
                        }
                        else
                        {
                            currentMoveSpeedMultiplier = runspeedmultiplier;
                        }
                    }
                    motor.inputJump = inputManager.CheckInput("Jump", KeyStages.Down);

                    motor.inputMoveDirection = this.transform.TransformDirection(dir * currentMoveSpeedMultiplier);
                }

                public void ChangeJoystick(bool b)
                {
                    inputManager.UseJoystick = b;
                }

                void OnApplicationQuit()
                {
                    inputManager.SaveConfig();
                }
            }
        }
    }
}