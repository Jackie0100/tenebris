using UnityEngine;
using System.Collections;

public class SimpleRotation : MonoBehaviour
{
	public float Speed = 1;
	// Update is called once per frame
	void Update ()
	{
		this.transform.Rotate(Vector3.forward, Speed);
	}
}
