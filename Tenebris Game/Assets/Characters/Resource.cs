﻿using System;

public enum Resource
{
    Health, Mana, Energy, Rage, Focus, Charges, Balance, Combo_Point, Holy_Power, Dark_Matters
}