using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour
{
    private Resource Resource1 = Resource.Health;
    public Resource Resource2 { get; set; }
    public Resource Resource3 { get; set; }
    public int MaxResource1 { get; set; }
    public int CurResource1 { get; set; }
    public int MaxResource2 { get; set; }
    public int CurResource2 { get; set; }
    public int MaxResource3 { get; set; }
    public int CurResource3 { get; set; }
    public int Level { get; set; }
    public string Name { get; set; }
    public string Title { get; set; }


    public bool ApplyDamage(int dam)
    {
        CurResource1 -= dam;
        return IsDead();
    }

    public void ApplyHeal(int heal)
    {
        CurResource1 += heal;
        if (CurResource1 > MaxResource1)
        {
            CurResource1 = MaxResource1;
        }
    }

    public bool IsDead()
    {
        if (CurResource1 <= 0)
        {
            return true;
        }
        return false;
    }
}
